# Role: __nginx-ingress__

### Description

Deploys the nginx Ingress Controller

### Requirements

+ __Default SSL Cert__

  To facilitate HTTPS it is possible to use a default certificate for every ingress. This certificate
  has to be present in the cluster.

  ```
    kubectl create namespace <<NGINX_NAMESPACE>>
    kubectl create secret -n <<NGINX_NAMESPACE>> tls <<SECRET_NAME>> --key ./privkey1.pem --cert ./fullchain1.pem

  ```

### Role Variables

+ __kubecontext__

  kubecontext as set in the ansible toolbox kubectl, should be set when running the image

+ __default_ssl_certificate_secret__

  the name of the default_ssl_certificate_secret object in k8s

+ __config.nginx_ingress.namespace__

  the namespace

+ __config.nginx_ingress.nodes__

  the nodes that the daemonset should be deployed on

+ __config.nginx_ingress.http_port__

  http ingress port

+ __config.nginx_ingress.https_port__

  https ingress port

+ __config.nginx_ingress.use_proxy_protocol__

  is there a proxy?
